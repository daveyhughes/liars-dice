import os

def main():
    pid = os.fork()

    if pid == 0:
        os.setgid(999)
        os.setuid(999)
        while True:
            pass


if __name__ == "__main__":
    main()