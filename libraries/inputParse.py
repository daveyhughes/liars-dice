from gameFormat import formatLine

class parseError(Exception):
    pass

class registerError(parseError):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

class deleteError(parseError):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

class inParser:
    def __init__(self):
        self.specialCmds = {}
        self.cmdList = []

    def registerCmd(self, cmd, func, desc="", options=[], args=[]):
        if cmd not in self.specialCmds:
            self.specialCmds[cmd] = (func, desc, options, args)
            self.cmdList.append(cmd)
            return None

        raise registerError("Command already registered")

    def deleteCmd(self, cmd):
        if cmd in self.specialCmds:
            self.specialCmds.pop(cmd, None)
            self.cmdList.remove(cmd)
            return None

        raise deleteError("Command not registered")

    def deleteAll(self):
        self.specialCmds = {}
        self.cmdList = []
        return None

    def getInput(self, phrase=""):
        cmd = input(phrase)
        if cmd in self.specialCmds:
            return self.specialCmds[cmd][0](*self.specialCmds[cmd][3])
        return cmd

    def listCmds(self):
        retval = []
        for cmd in self.cmdList:
            retval.append((cmd, self.specialCmds[cmd][1]))
        return retval

    def printCmds(self):
        for cmd in self.cmdList:
            cmdOptions = ""
            for option in self.specialCmds[cmd][2]:
                cmdOptions += option

            if cmdOptions != "":
                cmdOptions = " <" + cmdOptions + ">"

            print(formatLine("[" + cmd + "]" + cmdOptions, "indent-l1"))
            print(formatLine(self.specialCmds[cmd][1], "indent-l2"))

def test(self):
    self.getInput()

def test2():
    print("GG")

if __name__ == "__main__":
    newParser = inParser()

    newParser.registerCmd('h', test, "description 1", "player", args = [newParser])
    newParser.registerCmd('q', test2, "description 2")

    newParser.printCmds()

    testparse = newParser.getInput("what is your input: ")

    if (testparse): print(testparse)

    newParser.deleteCmd('h')
    newParser.deleteCmd('q')

