import sys

sys.path.insert(0, "../scripts")

from const import CONST

def formatLine(line, formatting):
    sizes = CONST.FORMATTING
    width = sizes["width"]
    space = " "

    if formatting == "center":
        return addPadding(line)
    elif formatting == "marquee":
        return addMarquee(line)
    elif formatting == "indent-l1":
        return int(sizes["indent-l1"] * width) * space + line
    elif formatting == "indent-l2":
        return int(sizes["indent-l2"]) * space + formatLine(line, "indent-l1")
    elif formatting == "none":
        return line
    elif formatting == "clear":
        print(chr(27) + "[2J")
        return line

def addPadding(line):
    sizes = CONST.FORMATTING
    width = sizes["width"]
    length = len(line)
    space = " "

    return (int((width - length) / 2) * space + line)

def addMarquee(line):
    sizes = CONST.FORMATTING
    width = sizes["width"]
    length = len(line)
    space = " "
    unitLeft = "-="
    unitRight= "=-"
    padding = int((width - length) / 4)

    return (padding * unitLeft + space + line + space + padding * unitRight)