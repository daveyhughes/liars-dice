import subprocess
from subprocess import PIPE
import os
import stat
import time

childPath = "./testChild.py"
APIPath = "../API/APIhelper"

def startChild(childPath):
    if os.path.isfile(childPath):
        os.chmod(childPath, stat.S_IRWXU | stat.S_IXGRP | stat.S_IXOTH)

        return(subprocess.Popen(["python3", childPath], bufsize=1, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE))

    return None


if __name__ == "__main__":
    child = startChild(childPath)

    for i in range(10):
        childIn = input()
        if (childIn == "q"):
            child.poll()
            break

        child.stdin.write(childIn)
        child.stdin.write("\n")
        print(child.stdout.readline(), end="")
