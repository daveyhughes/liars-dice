import random
import string
total_rolls = [1, 2, 3, 4, 5, 6]
start_num_of_dice = 5
num_of_players = 2.0
player1 = []
player2 = []
log1 = []
log2 = []
player1_num_dice = start_num_of_dice
player2_num_dice = start_num_of_dice
bet1 = []
bet2 = []

turn = 0
def properSyntax(current_bet):
    return (type(current_bet) is str and current_bet.isalpha() and current_bet.lower() == "bs" and turn) or (type(current_bet) == list and isinstance(current_bet, list) and len(current_bet) == 2 and current_bet[1] in total_rolls and (all(isinstance(item, int) for item in current_bet)))

def checkBet(previous_bet, current_bet):
    return properSyntax(current_bet) and (type(current_bet) is str or previous_bet == [] or (previous_bet[0] < current_bet[0] or (previous_bet[0] == current_bet[0] and previous_bet[1] < current_bet[1])))

def player1Turn():
    global bet1
    global bet2
    global player2_num_dice
    global player1_num_dice
    print("It's your turn! Make a bet with the following syntax: 6 dice of fours = [6,4], or call BS by typing BS")
    print("The previous round, the computer bet: %s" % bet2)
    while not checkBet(bet2, bet1):
        bet1 = input()

    if bet1 == "bs":
        print("You call BS!")
        print("Player 1's hand: %s" % player1)
        print("Player 2's hand: %s" % player2)
        result = player1.count(bet2[1]) + player2.count(bet2[1]) + player1.count(1) +  player2.count(1)
        if result < bet2[0]:
            print("Player 1 wins the round. The actual amount was %s" % [result, bet2[1]])
            player2_num_dice -= 1
            if player2_num_dice == 0:
                print("Player 1 wins!")
                print("gg")
            else:
                bet1 = []
                bet2 = []
                newRound()

        else:
            print("Player 2 wins the round. The actual amount was %s" % [result, bet2[1]])
            player1_num_dice -= 1
            if player1_num_dice == 0:
                print("Player 2 wins!")
                print("gg")
            else:
                bet1 = []
                bet2 = []
                newRound()
    else:
        log1.append(bet1)
        player2Turn()

def player2Turn():
    global player1_num_dice
    global player2_num_dice
    global bet1
    global bet2

    related2 = [x for x in player2 if x == bet1[1]]
    print(related2)
    expected = int(round(player1_num_dice / 3.0 + len(related2)))

    if bet1[0] < expected:
        bet2 = [expected + 1, bet1[1]]
        log2.append(bet2)
        player1Turn()
    else:
        print("Player 2 calls BS!")
        print("Player 1's hand: %s" % player1)
        print("Player 2's hand: %s" % player2)
        result = player1.count(bet1[1]) + player2.count(bet1[1]) + player1.count(1) +  player2.count(1)
        if result < bet1[0]:
            print("Player 2 wins the round. The actual amount was %s" % [result, bet1[1]])
            player1_num_dice -= 1
            if player1_num_dice == 0:
                print("Player 2 wins!")
                print("gg")
            else:
                bet1 = []
                bet2 = []
                newRound()
        else:
            print("Player 1 wins the round. The actual amount was %s" % [result, bet1[1]])
            player2_num_dice -= 1
            if player2_num_dice == 0:
                print("Player 1 wins!")
                print("gg")
            else:
                bet1 = []
                bet2 = []
                newRound()

def newRound():
    global player1_num_dice
    global player2_num_dice
    global bet1
    global bet2

    player1 = []
    player2 = []
    for i in range(0, player1_num_dice):
        player1.append(random.choice(total_rolls))

    for i in range(0, player2_num_dice):
        player2.append(random.choice(total_rolls))

    print("Welcome to Liar's Dice!")
    print("Here is your hand: %s" % player1)
    print("It's your turn! Make a bet with the following syntax: 6 dice of fours = [6,4]")
    while True:
        bet1_0 = int(input())
        bet1_1 = int(input())
        valid = checkBet(bet2, bet1)
        print(valid)
        print(properSyntax(bet1))
        if valid:
            break;
    log1.append(bet1)
    player2Turn()


newRound()