import csv
import itertools

testPath = "../data/test.csv"
testDict = [{"path" : testPath,
            "gameNum" : 1,
            "data" : ["Game"]}]

def extractGameData(path, game, tag, tag2):
    startTag = []
    endTag = []
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            if row == [tag]:
                startTag.append(reader.line_num)
            elif row == [tag2]:
                endTag.append(reader.line_num)
            elif row == ['Game', str(game)]:
                if tag == "<GAME_META>" or tag == "<GAME>":
                    startline = reader.line_num + 3
                elif tag == "<ALL_ROUNDS>":
                    startline = reader.line_num + 10
        if startline >= startTag[-1]:
            start = startTag[-1]
            end = endTag[-1]
        elif startline <= startTag[0]:
            start = startTag[0]
            end = endTag[0]
        else:
            i = 0
            while startline > startTag[i]:
                i += 1
            start = startTag[i - 1]
            end = endTag[i - 1]
        result = []
        for line in range(start, end - 1):
            csvfile.seek(0)
            sectline = next(itertools.islice(csv.reader(csvfile), line, None))
            if len(sectline) > 1:
                result.append(sectline)
        return result

#TODO: find specific rounds
def extractRoundData(path, game, tag, tag2, roundnum):
    startTag = []
    endTag = []
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            if row == [tag]:
                startTag.append(reader.line_num)
            elif row == [tag2]:
                endTag.append(reader.line_num)
            elif row == ['Game', str(game)]:
                if tag == "<ROUND>":
                    
                    startline = reader.line_num + 3
                elif tag == "<ALL_ROUNDS>":
                    startline = reader.line_num + 10
        if startline >= startTag[-1]:
            start = startTag[-1]
            end = endTag[-1]
        elif startline <= startTag[0]:
            start = startTag[0]
            end = endTag[0]
        else:
            i = 0
            while startline > startTag[i]:
                i += 1
            start = startTag[i - 1]
            end = endTag[i - 1]
        result = []
        for line in range(start, end - 1):
            csvfile.seek(0)
            sectline = next(itertools.islice(csv.reader(csvfile), line, None))
            if len(sectline) > 1:
                result.append(sectline)
        return result

def extractAllRounds(path, game):
    return extractGameData(path, game, "<ALL_ROUNDS>", "</ALL_ROUNDS>")

def extractGameMeta(path, game):
    return extractGameData(path, game, "<GAME_META>", "</GAME_META>")

def extractTotal(path, game):
    return extractGameData(path, game, "<GAME>", "</GAME>")

def extractGame(myArr):
    data = []
    for dicts in myArr:
        path = dicts.get("path")
        game = dicts.get("gameNum")
        if "GameMeta" in dicts.get("data"):
            data = data + ["Game " + str(game) + ": GameMeta", extractGameMeta(path, game)]
        if "AllRounds" in dicts.get("data"):
            data = data + ["Game " + str(game) + ": All Rounds", extractAllRounds(path, game)]
        if "Game" in dicts.get("data"):
            data = data + ["Game " + str(game) + ": Total", extractTotal(path, game)]
    return data
if __name__ == "__main__":
    print(extractGame(testDict))



