import re
import sys

sys.path.insert(0, "../libraries")

import inputParse
from gameFormat import formatLine

import glob
from const import CONST

class notImplemented(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value

class controlFlow(Exception):
    pass

class eFinishEntry(controlFlow):
    pass

class eCancelEntry(controlFlow):
    pass

class eListPlayers(controlFlow):
    pass

class eRemovePlayer(controlFlow):
    pass

class eRemoveAll(controlFlow):
    pass

class playerClass:
    def __init__(self, index, name, isComputer, file):
        self.index = index
        self.name = name
        self.isComputer = isComputer
        self.file = file

    def getAllInfo(self):
        return (self.index, self.name, self.isComputer, self.file)

    def getComputer(self):
        return self.isComputer

def finishEntry():
    raise eFinishEntry()

def addFromConfig():
    raise notImplemented("addFromConfig")

def cancelEntry():
    raise eCancelEntry()

def listPlayers():
    raise eListPlayers

def removePlayer():
    raise eRemovePlayer()

def removeAll():
    raise eRemoveAll()

def quitGame():
    sys.exit()

def showHelp(parser):
    # info = CONST.REGI_PLAYER
    spacing = CONST.FORMATTING

    marqueeLines = ["player entry", "liar's dice"]
    centeredLines = ["2 players minimum must join to start game"]
    noFormat = ["Options:"]

    print(formatLine("", "clear"))
    print(formatLine("Player Entry", "marquee"))
    print(formatLine("2 players minimum must join to start game", "center"))
    print()
    print(formatLine("Options:", "none"))

    parser.printCmds()

    print(formatLine("Liar's Dice", "marquee"))
    print()

def showVerboseHelp():
    raise notImplemented("showVerboseHelp")

def getNextPlayer(parser):
    isComputer = False
    name = None
    file = None

    while True:
        msg = parser.getInput("Human [0] or computer [1]: ")
        if msg and all(c in "01" for c in msg):
            msg = int(msg)
            if msg == 1:
                isComputer = True
            if msg < 2:
                break

    while True:
        name = parser.getInput("Name: ")
        if name:
            break

    if isComputer:
        while True:
            file = parser.getInput("File: ")
            if file:
                break

    return {"name" : name, "isComputer" : isComputer, "file" : file}

def convertPlayerList(players):
    playerObjects = []
    n = 0

    for p in players:
        playerObjects.append(playerClass(n, p["name"], p["isComputer"], p["file"]))
        n += 1

    return playerObjects

def registerPlayers():
    playerList = []
    playerObjects = []
    entryParser = inputParse.inParser()

    entryParser.registerCmd("-d", finishEntry, "finish adding players")
    entryParser.registerCmd("-s", addFromConfig, "add players from config file", "file")
    entryParser.registerCmd("-r", cancelEntry, "cancel current player entry")
    entryParser.registerCmd("-l", listPlayers, "list entered players")
    entryParser.registerCmd("-k", removePlayer, "remove player by name", "player")
    entryParser.registerCmd("-c", removeAll, "clear all entered players")
    entryParser.registerCmd("-q", quitGame, "quit game")
    entryParser.registerCmd("-h", showHelp, "show this help menu", args=[entryParser])
    entryParser.registerCmd("-v", showVerboseHelp, "show more verbose help menu")

    showHelp(entryParser)

    while True:
        try:
            playerList.append(getNextPlayer(entryParser))
        except eFinishEntry:
            if len(playerList) < 2:
                print("2 players minimum needed to start game")
                continue
            playerObjects = convertPlayerList(playerList)
            break
        except eCancelEntry:
            continue
        except eListPlayers:
            if playerList:
                for player in playerList:
                    print(player)
            else:
                print("No players")
        except eRemovePlayer:
            if playerList == []:
                print("No players currently added to the game.")
                continue

            removeName = input("Enter name to remove player, or enter no name to cancel removal: ")
            if removeName:
                removeList = []

                for index, player in enumerate(playerList):
                    if player["name"] == removeName:
                        removeList.append((player, index))

                l = len(removeList)
                if not l:
                    print("Player specified not in game.")
                elif l == 1:
                    del playerList[removeList[0][1]]
                else:
                    printRemoveList = ""
                    for player in removeList:
                        printRemoveList += str(player[0]["name"]) + " [" + str(player[1]) + "] "

                    print("Multiple players with that name. Which one to remove? Or enter nothing to cancel removal.")
                    while True:
                        num = input(printRemoveList + ": ")

                        if not num:
                            break
                        elif all(c in "0123456789" for c in num):
                            num = int(num)
                            try:
                                del playerList[removeList[num][1]]
                                break
                            except IndexError:
                                pass

                        print("Please enter a valid number corresponding to a player, or enter nothing to cancel removal.")

            else:
                continue

        except eRemoveAll:
            playerList = []
        except notImplemented as e:
            print("Not Implemented: " + str(e))

    return playerObjects