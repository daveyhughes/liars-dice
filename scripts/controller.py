import random
import time

def createController(fromMain, players):
    master = missionControl(fromMain, players)

    return master

class missionControl:
    def __init__(self, fromMain, players):
        self.uniqueId = random.random()
        for member in fromMain:
            self.fileObjs = member["files"]
            self.paths = member["paths"]

        num = len(players)
        numH = 0

        for p in players:
            if not p.getComputer:
                numH += 1

        self.metadata = {
            "numPlayers" : num,
            "numHumans" : numH,
            "numDice" : 6 * num
        }

        self.playerList = players
        self.playOrder = []

        self.shufflePlayOrder()


    ### methods to change controller information ###

    def shufflePlayOrder(self):
        order = [i for i in range(self.metadata["numPlayers"])]

        random.seed()
        random.shuffle(order)

        self.playOrder = order


    ### methods to retreive attributes ###

    def getMetadata(self, key):
        return self.metadata[key]

    def getNumPlayers(self):
        return self.metadata["numPlayers"]

    def getNumHumans(self):
        return self.metadata["numHumans"]

    def getNumDice(self):
        return self.metadata["numDice"]

    def getPlayerObject(self):
        pass

    def getNamedPlayerOrder(self):
        pass

    def getNumberedPlayOrder(self):
        return self.playOrder


    ### methods to change attributes ###

    def setPlayerNum(self, manner, num):
        if manner == "set":
            self.metadata["numPlayers"] = num
            return None
        elif manner == "add":
            self.metadata["numPlayers"] += num
            return None
        elif manner == "sub":
            self.betadata["numPlayers"] -= num
            return None
        else:
            return "manner must be ['set', 'add', 'sub']"

    def setNumHumans(self, manner, num):
        if manner == "set":
            self.metadata["numHumans"] = num
            return None
        elif manner == "add":
            self.metadata["numHumans"] += num
            return None
        elif manner == "sub":
            self.betadata["numHumans"] -= num
            return None
        else:
            return "manner must be ['set', 'add', 'sub']"

    def setPlayOrder(self, order):
        self.playOrder = order
