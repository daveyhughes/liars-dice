import os

curDir = os.path.dirname(__file__)
logDir = os.path.join(curDir, 'logs/')
movesPath = os.path.join(logDir, 'moves.txt')
debugPath = os.path.join(logDir, 'debug.txt')

def constant(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        return f()
    return property(fget, fset)

class _const:
    @constant
    def VERSION():
        info = {
            "name" : "version",
            "value" : "0.1.1"
        }
        return info["value"]

    @constant
    def SLEEP():
        info = {
            "name" : "sleep",
            "duration" : .2
        }
        return info["duration"]

    @constant
    def FORMATTING():
        info = {
            "name" : "formatting",
            "width" : 45,
            "indent-l1" : 1/12,
            "indent-l2" : 2,
            "clear" : chr(27) + "[2J"
        }
        return info

    @constant
    def LOG_PATHS():
        info = {
            "name" : "log_paths",
            "dir" : logDir,
            "moveHistory" : movesPath,
            "debug" : debugPath
        }
        return {
            key: info[key] for key in ["dir", "moveHistory", "debug"] if key in info
        }

    @constant
    def WELCOME():
        info = {
            "name" : "welcome",
            "text" : [
                "Welcome to Liar's Dice!",
                "",
                "Created by Mingu Kim and Davey Hughes",
                "March 18, 2016, version: " + str(CONST.VERSION)
            ]
        }
        return info["text"]

CONST = _const()